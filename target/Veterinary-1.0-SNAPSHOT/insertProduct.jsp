<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/style.css" type="text/css"/>
        <title>Insert Product</title>
    </head>
    <body>
        <h1 class="register-title">Registrar Productos</h1>
        <form class="register" id="form-one" method="post" action="InsertProductController">
          <div class="register-switch">
            <input type="radio" name="type" value="medicamento" id="medication" class="register-switch-input" checked required>
            <label for="medication" class="register-switch-label">Medicamento</label>
            <input type="radio" name="type" value="regular" id="regular" class="register-switch-input" required>
            <label for="regular" class="register-switch-label">Regular</label>
          </div>
          <input type="text" class="register-input" name="description" placeholder="Descripción" required>
          <input type="number" class="register-input" name="price" placeholder="Precio" required>
          <button type="submit" class="register-button" form="form-one" name="status" value="disponible">Registrar</button>
        </form>
        <button class="button back-button" onclick="location.href='administrator.jsp'" type="button">Regresar</button><br>
         <c:if test="${not empty message}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${message}");
            </script>
        </c:if>
    </body>
</html>
