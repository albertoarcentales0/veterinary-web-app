<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/index.css" type="text/css"/>
        <title>Index</title>
    </head>
    <body>
        <div class="user">
            <form id="form-one" class="form" method="post" action="SignUpController">
                <div>
                    <input id="title" name="employeeRole" type="text" value="Administrador" readonly="readonly" tabindex="-1"/>
                </div>
                <div>
                    <input class="form-input" name="employeeName" type="text" placeholder="Usuario" required />
                </div>
                <div>
                    <input class="form-input"  name="employeePassword" type="password" placeholder="Contraseña" required />
                </div>
                <button class="button" type="submit" form="form-one" name="sender" value="system">Registrar</button>
        </form>
        </div>
    </body>
</html>
