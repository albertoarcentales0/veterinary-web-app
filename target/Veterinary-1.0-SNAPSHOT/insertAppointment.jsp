<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/style.css" type="text/css"/>
        <title>Insert Appointment</title>
    </head>
    <body>
        <h1 class="register-title">Turno Médico</h1>
        <form class="register" id="form-one" method="post" action="InsertAppointmentController">
            <input type="text" class="register-input" name="ownerName" placeholder="Nombre" required>
            <input type="number" class="register-input" name="phoneNumber" placeholder="Número telefónico" required>
            <input type="text" class="register-input" name="petName" placeholder="Nombre de la mascota" required>
            <input type="time" class="register-input" name="time" required>
            <select class="register-select register-input" name="type">
                <option class="register-option" value="tortuga">Tortuga</option>
                <option class="register-option" value="gato">Gato</option>
                <option class="register-option" value="perro">Perro</option>
                <option class="register-option" value="canario">Canario</option>
            </select>
            <button type="submit" class="register-button" form="form-one" name="status" value="pendiente">Registrar</button>
        </form>
        <button class="button back-button" onclick="location.href='receptionist.jsp'" type="button">Regresar</button><br>
        <c:if test="${not empty message}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${message}");
            </script>
        </c:if> 
    </body>
</html>
