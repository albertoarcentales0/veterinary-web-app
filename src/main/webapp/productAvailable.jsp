<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/style.css" type="text/css"/>
        <title>Medications Available</title>
    </head>
    <body>
        <c:choose>
            <c:when test="${empty products}">
                <div class="div-title-page">
                    <h1 class="h1-title-page">NO HAY PRODUCTOS DISPONIBLES</h1>
                </div>
            </c:when>
            <c:otherwise>
                <div class="container">
                    <table>
                        <thead>
                            <tr>
				<th>ID</th>
				<th>Descripción</th>
				<th>Tipo</th>
				<th>Precio</th>
				<th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${products}" var="product">
                                <tr>
                                    <td>${product.productID}</td>
                                    <td>${product.description}</td>
                                    <td>${product.type}</td>
                                    <td>${product.price}</td>
                                    <td>${product.status}</td>
                                </tr>
                             </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
        <button class="button back-button" onclick="location.href='administrator.jsp'" type="button">Regresar</button><br>
    </body>
</html>
