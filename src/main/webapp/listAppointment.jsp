<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/style.css" type="text/css"/>
        <title>List Appointments</title>
    </head>
    <body>
        <form id="form-one" method="post" action="TreatPetController"> 
            <c:choose>
                <c:when test="${empty appointments}">
                    <div class="div-title-page">
                        <h1 class="h1-title-page">NO HAY TURNOS</h1>
                    </div>
                </c:when>
                <c:otherwise> 
                    <div class="div-title-page">
                        <h1 class="h1-title-page">TURNOS PENDIENTES</h1>
                    </div>
                    <div class="container">
                        <table>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nombre</th>
                                    <th>Número telefónico</th>
                                    <th>Nombre de la mascota</th>
                                    <th>Tipo de mascota</th>
                                    <th>Hora</th>
                                    <th>Estado del turno</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${appointments}" var="appointment">
                                    <tr>
                                        <td><input type="checkbox" name="appointments" value="${appointment.appointmentID}"></td>
                                        <td>${appointment.ownerName}</td>
                                        <td>${appointment.phoneNumber}</td>
                                        <td>${appointment.petName}</td>
                                        <td>${appointment.type}</td>
                                        <td>${appointment.time}</td>
                                        <td>${appointment.status}</td>
                                    </tr>
                                 </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="action-button" name="status" value="atendido" form="form-one">Atender</button>      
                </c:otherwise>
            </c:choose>
        </form>	
        <button class="button back-button" onclick="location.href='veterinarian.jsp'" type="button">Regresar</button><br>
        <c:if test="${!message.equals('notAppointments') && message != null}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${message}");
            </script>
        </c:if>
    </body>
</html>
