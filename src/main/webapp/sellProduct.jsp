<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/style.css" type="text/css"/>
        <title>Sell a Product</title>
    </head>
    <body>
        <form id="form-one" method="post" action="SellProductController"> 
            <c:choose>
            <c:when test="${empty products}">
                <div class="div-title-page">
                    <h1 class="h1-title-page">NO HAY PRODUCTOS PARA VENDER</h1>
                </div>
            </c:when>
            <c:otherwise>
                <div class="div-title-page">
                    <h1 class="h1-title-page">PRODUCTOS DISPONIBLES</h1>
                </div>
                <div class="container">
                    <table>
                        <thead>
                            <tr>
				<th></th>
				<th>Descripción</th>
				<th>Tipo</th>
				<th>Precio</th>
				<th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${products}" var="product">
                                <tr>
                                    <td><input type="checkbox" name="products" value="${product.productID}"></td>
                                    <td>${product.description}</td>
                                    <td><input type="hidden" name="typeOfProduct" value="${product.type}"/>${product.type}</td>
                                    <td>${product.price}</td>
                                    <td><input type="hidden" name="available" value="${product.status}"/>${product.status}</td>
                                </tr>
                             </c:forEach>
                        </tbody>
                    </table>
                </div>
                <button type="submit" class="action-button" name="status" value="vendido" form="form-one">Vender</button>
                <input type="hidden" name="backPage" value="${backPage}"/>
            </c:otherwise>
        </c:choose>
        </form>	
        <button class="button back-button" onclick="location.href='${backPage}'" type="button">Regresar</button><br>
        <c:if test="${!message.equals('notProducts') && message != null}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${message}");
            </script>
        </c:if>
    </body>
</html>
