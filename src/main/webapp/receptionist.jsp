<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/style.css" type="text/css"/>
        <title>Receptionist</title>
    </head>
    <body>
        <button class="button sign-out-button" onclick="location.href='signIn.jsp'" type="button">Cerrar Sesión</button><br>
         <div class="div-title-page">
            <h1 class="h1-title-page">RECEPCIÓN</h1>
        </div>
        <div class="main">
            <div class="secondary">
                <form id="form-one" method="post" action="ListProductSaleController">
                    <button class="button" type="submit" name="typeOfProduct" value="regular" form="form-one">Vender regulares</button>
                    <input type="hidden" name="sender" value="receptionist">
                </form>
                <button class="button" onclick="location.href='insertAppointment.jsp'" type="button">Agendar turnos</button><br>
            </div>
        </div>
        </body>
    
</html>
