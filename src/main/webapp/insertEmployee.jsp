<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/style.css" type="text/css"/>
        <title>Insert Employee</title>
    </head>
    <body> 
        <h1 class="register-title">Registrar Empleados</h1>
        <form class="register" id="form-one" method="post" action="SignUpController">
            <div class="register-switch">
              <input type="radio" name="employeeRole" value="recepcionista" id="receptionist" class="register-switch-input" checked required>
              <label for="receptionist" class="register-switch-label">Recepcionista</label>
              <input type="radio" name="employeeRole" value="veterinario" id="veterinary" class="register-switch-input" required>
              <label for="veterinary" class="register-switch-label">Veterinario</label>
            </div>
            <input type="text" class="register-input" name="employeeName" placeholder="Usuario" required>
            <input type="password" class="register-input" name="employeePassword" placeholder="Contraseña" required>
            <button type="submit" class="register-button" form="form-one" name="sender" value="administrator">Registrar</button>
        </form>
        <button class="button back-button" onclick="location.href='administrator.jsp'" type="button">Regresar</button><br>
        <c:if test="${not empty message}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${message}");
            </script>
        </c:if>
    </body>
</html>
