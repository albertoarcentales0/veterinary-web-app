
create database if not exists veterinary;

use veterinary;

create table if not exists employees (
 employeeID int  AUTO_INCREMENT,
 employeeName nvarchar(30)  not null,
 employeePassword nvarchar(30)  not null,
 employeeRole nvarchar(30)  not null,
 PRIMARY KEY(employeeID)
);

insert into employees(employeeName, employeePassword, employeeRole)
values ('re', 're', 'receptionist'),
       ('ad', 'ad', 'administrator'),
       ('ve', 've', 'veterinarian');


create table if not exists products (
 productID int  AUTO_INCREMENT,
 description nvarchar(30)  not null,
 type nvarchar(30)  not null,
 price int not null,
 status nvarchar(30)  not null,
 PRIMARY KEY(productID)
);

insert into products(description, type, price, status)
values ('product-one', 'regular', 20, 'available'),
       ('product-two', 'regular', 20, 'available'),
       ('product-one', 'medication', 20, 'available'),
       ('product-two', 'medication', 20, 'available');

create table if not exists appointments (
 appointmentID int  AUTO_INCREMENT,
 ownerName nvarchar(30)  not null,
 phoneNumber int not null,
 petName nvarchar(30) not null,
 type nvarchar(30)  not null,
 time time  not null,
 status nvarchar(30) not null,
 PRIMARY KEY(appointmentID)
);

insert into  appointments(ownerName, phoneNumber, petName,
             type, time, status)
values  ('Perl', 20435353, 'zas', 'Tortuga', '12:30:00', 'En espera'),
        ('Perl', 20435353, 'zas', 'Gato', '12:00:00', 'En espera'),
        ('Perl', 20435353, 'zas', 'Perro', '11:30:00', 'En espera'),
        ('Perl', 20435353, 'zas', 'Canario', '12:00:00', 'En espera');
