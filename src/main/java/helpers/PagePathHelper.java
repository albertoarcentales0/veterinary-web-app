package helpers;

import java.util.HashMap;
import java.util.Map;

public class PagePathHelper {
    
    Map<String, String> pagePath = null;   
    public PagePathHelper(){
        pagePath = new HashMap<>();	
        pagePath.put("recepcionista", "receptionist.jsp");
        pagePath.put("veterinario", "veterinarian.jsp");
        pagePath.put("signIn", "signIn.jsp");        
        pagePath.put("insertProduct", "insertProduct.jsp");
        pagePath.put("productAvailable", "productAvailable.jsp");
        pagePath.put("sellProduct", "sellProduct.jsp");
        pagePath.put("productSold", "productSold.jsp");
        pagePath.put("listAppointment", "listAppointment.jsp");
        pagePath.put("insertEmployee", "insertEmployee.jsp");
        pagePath.put("insertAppointment", "insertAppointment.jsp");
        pagePath.put("administrador", "administrator.jsp");
        pagePath.put("index", "index.jsp");
    }
    
    public String getPagePath(String key) {
      return pagePath.get(key);
    }
}
