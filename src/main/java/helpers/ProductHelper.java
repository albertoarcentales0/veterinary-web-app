package helpers;

import java.util.List;
import models.Product;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class ProductHelper {
    
    List <Product> products = null;
    public List<Product> getProducts(Session session, String typeOfProduct, String status) {
        String hql = "FROM Product p WHERE  p.type = :ty AND p.status = :st";
        Query query = session.createQuery(hql);
        query.setParameter("ty", typeOfProduct);
        query.setParameter("st", status);
        products = query.getResultList();
        return products;
    }
    
    public int deleteProduct(Session session, String[] productIDs, String status) {
        int numberOfRecordsAffected = 0;
        String hql = "UPDATE Product p SET status = :st WHERE  p.productID = :pr";
        Query query = session.createQuery(hql);
        for(String productID : productIDs) {
            query.setParameter("st", status);
            query.setParameter("pr", Integer.parseInt(productID));
            numberOfRecordsAffected += query.executeUpdate();
        }
        return numberOfRecordsAffected;
    }
}
