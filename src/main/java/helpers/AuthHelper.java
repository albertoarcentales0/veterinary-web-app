package helpers;

import java.util.List;
import models.Employee;
import models.Product;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class AuthHelper {
    
    List <Employee> employees = null;
    List <Product> products = null;
    
    public List<Employee> getEmployees(Session session, String employeeName, String employeePassword) {     
        String hql = "FROM Employee e WHERE e.employeeName = :en AND e.employeePassword = :ep";
        Query query = session.createQuery(hql);
        query.setParameter("en", employeeName);
        query.setParameter("ep", employeePassword);
        employees = query.getResultList();
        return employees;
    }
    
    public boolean isThereAProduct(Session session, String description, String type, int price, String status) {
        String hql = "FROM Product p WHERE p.description = :de AND p.type = :ty AND p.price = :pr AND p.status = :st";
        Query query = session.createQuery(hql);
        query.setParameter("de", description);
        query.setParameter("ty", type);
        query.setParameter("pr", price);
        query.setParameter("st", status);
        products = query.getResultList();
        boolean thereIsAProduct = !products.isEmpty();
        return thereIsAProduct;
    }
    
}
