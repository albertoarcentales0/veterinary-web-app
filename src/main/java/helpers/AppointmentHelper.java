package helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Appointment;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class AppointmentHelper {
    
    List <Appointment> appointments = null;
    public List<Appointment> getAppointments(Session session, String status) {
        String hql = "FROM Appointment a WHERE a.status = :st";
        Query query = session.createQuery(hql);
        query.setParameter("st", status);
        appointments = query.getResultList();
        return appointments;
    }
    
    public int deleteAppointment(Session session, String[] appointmentIDs, String status) {
        int numberOfRecordsAffected = 0;
        String hql = "UPDATE Appointment a SET status = :st WHERE  a.appointmentID = :ap";
        Query query = session.createQuery(hql);
        for(String appointmentID : appointmentIDs) {
            query.setParameter("st", status);
            query.setParameter("ap", Integer.parseInt(appointmentID));
            numberOfRecordsAffected += query.executeUpdate();
        }
        return numberOfRecordsAffected;
    }
    
    public String getTimeValidationMessage(String time) {
        /*Los turnos se dan desde las 9hs hasta las 18hs en
         intervalos de 30 minutos y con un receso de 12hs a 13.30hs*/
         /* 9 a 12 y de 13:30 18 */
         /* 9, 9:30, ...*/
        String response = "OK";
        try {
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
            Date horaIniPrimerBloque = timeFormatter.parse("9:00");
            Date horaFinPrimerBloque = timeFormatter.parse("12:00");
            Date horaIniSegundoBloque = timeFormatter.parse("13:30");
            Date horaFinSegundoBloque = timeFormatter.parse("18:00");
            Date formattedTime = timeFormatter.parse(time);
             
            if ( (horaIniPrimerBloque.compareTo(formattedTime) <= 0 && horaFinPrimerBloque.compareTo(formattedTime) >= 0)
                ||(horaIniSegundoBloque.compareTo(formattedTime) <= 0 && horaFinSegundoBloque.compareTo(formattedTime) >= 0) 
               ) {
                    long diferenciaMinutos =  (formattedTime.getTime() - horaIniPrimerBloque.getTime()) / 1000 / 60;
                    if (diferenciaMinutos % 30 > 0) {
                        response = "Los horarios se deben asignar cada 30 minutos";
                    }
            } else {
                response = "Hora sin atencion al publico";
            }
        }
        catch(ParseException e) {    
            response = "La hora no es válida";
        }        
        return response;
    }

    public boolean isTimeAvailable(Session session, String time) {
        boolean response = false;
        try {
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
            Date formattedTime = timeFormatter.parse(time);
            String hql = "FROM Appointment a WHERE a.time = :ti";
            Query query = session.createQuery(hql);
            query.setParameter("ti", formattedTime);
            appointments = query.getResultList();
            response = appointments.isEmpty();
        } catch (ParseException ex) {
            Logger.getLogger(AppointmentHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
    
}
