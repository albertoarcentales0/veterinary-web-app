package controllers;

import helpers.AppointmentHelper;
import helpers.PagePathHelper;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Appointment;
import models.HibernateUtil;
import org.hibernate.Session;

@WebServlet(name = "InsertAppointmentController", urlPatterns = {"/InsertAppointmentController"})
public class InsertAppointmentController extends HttpServlet {

    Appointment appointment = null;
    PagePathHelper pagePathHelper = null;
    AppointmentHelper appointmentHelper = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        try {
            appointment = new Appointment();
            pagePathHelper = new PagePathHelper();
            appointmentHelper = new AppointmentHelper(); 
            String page;
            String ownerName = request.getParameter("ownerName");
            int phoneNumber = Integer.parseInt(request.getParameter("phoneNumber"));
            String petName = request.getParameter("petName");
            String type = request.getParameter("type");
            String time = request.getParameter("time");
            String status = request.getParameter("status");
            Date dateTime;
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            dateTime = dateFormat.parse(time);       

            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                session.beginTransaction();
                String message = appointmentHelper.getTimeValidationMessage(time);
                boolean result = appointmentHelper.isTimeAvailable(session, time);
                if(message.equalsIgnoreCase("ok")) {
                    if(result) {
                        appointment.setOwnerName(ownerName);
                        appointment.setPhoneNumber(phoneNumber);
                        appointment.setPetName(petName);
                        appointment.setType(type);
                        appointment.setTime(dateTime);
                        appointment.setStatus(status);
                        session.save(appointment);
                        request.setAttribute("message", "El turno fue registrado");
                    } else {
                        request.setAttribute("message", "Horario no disponible");
                    }
                    
                } else {
                    request.setAttribute("message", message);
                }
                session.getTransaction().commit();
                session.close();
            }
            page = pagePathHelper.getPagePath("insertAppointment");
            RequestDispatcher rd = request.getRequestDispatcher(page);
            rd.forward(request, response);
        }   catch (ParseException ex) {
            Logger.getLogger(InsertAppointmentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
