package controllers;

import helpers.AuthHelper;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Employee;
import models.HibernateUtil;
import org.hibernate.Session;
import helpers.PagePathHelper;
import java.util.List;

@WebServlet(name = "SignUpController", urlPatterns = {"/SignUpController"})
public class SignUpController extends HttpServlet {

    private Employee employee = null;
    private PagePathHelper pagePathHelper = null;
    private AuthHelper authHelper = null;
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        employee = new Employee();
        pagePathHelper = new PagePathHelper();
        authHelper = new AuthHelper();
        boolean status = false;
        String page;
        String employeeName = request.getParameter("employeeName");
        String employeePassword = request.getParameter("employeePassword");
        String employeeRole = request.getParameter("employeeRole").toLowerCase();
        String sender = request.getParameter("sender");
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            //boolean isRegisteredEmployee = authHelper.isThereAnEmployee(session, employeeName, employeePassword);
            List<Employee> employees = authHelper.getEmployees(session, employeeName, employeePassword);
            if (employees.size() > 0) {
                status = true;
                request.setAttribute("message", "El usuario ya existe");
                page = pagePathHelper.getPagePath("index");
            } else {
                employee.setEmployeeName(employeeName);
                employee.setEmployeePassword(employeePassword);
                employee.setEmployeeRole(employeeRole);
                session.save(employee); 
                request.setAttribute("message", "El usuario fue registrado");
            }
            switch(sender) {
                case "administrator":
                    page = pagePathHelper.getPagePath("insertEmployee");
                    break;
                default:
                    if(status) {
                        page = pagePathHelper.getPagePath("index");    
                    } else {
                        page = pagePathHelper.getPagePath("signIn");
                    }
                    break;
            }
            session.getTransaction().commit();
            session.close();  
        }
        RequestDispatcher rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }
}
