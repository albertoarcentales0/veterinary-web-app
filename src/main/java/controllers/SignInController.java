package controllers;

import helpers.AuthHelper;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.HibernateUtil;
import org.hibernate.Session;
import helpers.PagePathHelper;
import java.util.List;
import models.Employee;

@WebServlet(name = "SignInController", urlPatterns = {"/SignInController"})
public class  SignInController extends HttpServlet {
    
    private PagePathHelper pagePathHelper = null;
    private AuthHelper authHelper = null;
  
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        pagePathHelper = new PagePathHelper();
        authHelper = new AuthHelper();
        String employeeName = request.getParameter("employeeName");
        String employeePassword = request.getParameter("employeePassword");
        String page;
        String employeeRole;
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            List<Employee> employees = authHelper.getEmployees(session, employeeName, employeePassword);
            if (employees.size() > 0) {
                employeeRole = employees.get(0).getEmployeeRole();
                page = pagePathHelper.getPagePath(employeeRole);
            } else {
                page = pagePathHelper.getPagePath("signIn");
                request.setAttribute("message", "El usuario no existe");
            }
            session.getTransaction().commit();
            session.close();
        }       
        RequestDispatcher rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }
}

