package controllers;

import helpers.AppointmentHelper;
import helpers.PagePathHelper;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Appointment;
import models.HibernateUtil;
import org.hibernate.Session;

@WebServlet(name = "TreatPetController", urlPatterns = {"/TreatPetController"})
public class TreatPetController extends HttpServlet {

    private PagePathHelper pagePathHelper = null;
    private AppointmentHelper appointmentHelper = null;
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        String [] appointmentIDs = request.getParameterValues("appointments");
        String status = request.getParameter("status");
        pagePathHelper = new PagePathHelper();
        String page;
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            if(appointmentIDs != null) {
                appointmentHelper = new AppointmentHelper();
                int numberOfRecordsAffected = appointmentHelper.deleteAppointment(session, appointmentIDs, status);
                request.setAttribute("message", numberOfRecordsAffected +" turno(s) atendido(s)");
                status = "pendiente";
                List <Appointment> appointments = appointmentHelper.getAppointments(session, status);
                if (!appointments.isEmpty()) {
                    request.setAttribute("appointments", appointments);
                } 
            } else {
                status = "pendiente";
                List <Appointment> appointments = appointmentHelper.getAppointments(session, status);
                if (!appointments.isEmpty()) {
                    request.setAttribute("appointments", appointments);
                }
            }
            page = pagePathHelper.getPagePath("listAppointment");
            session.getTransaction().commit();
            session.close();  
        }       
        RequestDispatcher rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }
}
