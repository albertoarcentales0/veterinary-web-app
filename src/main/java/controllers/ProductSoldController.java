package controllers;

import helpers.PagePathHelper;
import helpers.ProductHelper;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.HibernateUtil;
import models.Product;
import org.hibernate.Session;

public class ProductSoldController extends HttpServlet {

    private PagePathHelper pagePath = null;
    private ProductHelper productHelper = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        pagePath = new PagePathHelper();
        productHelper = new ProductHelper();
        String page;
        String status = "vendido";
        String typeOfProduct = request.getParameter("typeOfProduct");
        if(typeOfProduct.equals("regular")) {
            request.setAttribute("product", "Regulares");
        } else {
            request.setAttribute("product", "Medicaciones");
        }
       
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
           
            List <Product> products = productHelper.getProducts(session, typeOfProduct, status);
            if (products.isEmpty()) {
                request.setAttribute("message", "No hay productos");
            } else {
                request.setAttribute("products", products);
            }
            page = pagePath.getPagePath("productSold");
            session.getTransaction().commit();
            session.close();  
        }       
        RequestDispatcher rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }
}
