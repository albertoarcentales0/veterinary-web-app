package controllers;

import helpers.PagePathHelper;
import helpers.ProductHelper;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.HibernateUtil;
import models.Product;
import org.hibernate.Session;

@WebServlet(name = "SellProductController", urlPatterns = {"/SellProductController"})
public class SellProductController extends HttpServlet {
    
    private PagePathHelper pagePathHelper = null;
    private ProductHelper productHelper = null;
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String [] productIDs = request.getParameterValues("products");
        String typeOfProduct = request.getParameter("typeOfProduct");
        String status = request.getParameter("status");
        String backPage = request.getParameter("backPage");
        pagePathHelper = new PagePathHelper();
        String page;
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            productHelper = new ProductHelper();
            int numberOfRecordsAffected = productHelper.deleteProduct(session, productIDs, status);
            if (numberOfRecordsAffected > 0) {
                request.setAttribute("message", numberOfRecordsAffected +" producto(s) vendido(s)");
            }
            status = "disponible";
            List <Product> products = productHelper.getProducts(session, typeOfProduct, status);
             if (!products.isEmpty()) {
                request.setAttribute("products", products);
            } 
            page = pagePathHelper.getPagePath("sellProduct");
            session.getTransaction().commit();
            session.close();  
        }       
        request.setAttribute("backPage", backPage);
        RequestDispatcher rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }
}
