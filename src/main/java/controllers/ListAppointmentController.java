package controllers;

import helpers.AppointmentHelper;
import helpers.PagePathHelper;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Appointment;
import models.HibernateUtil;
import org.hibernate.Session;

@WebServlet(name = "ListAppointmentController", urlPatterns = {"/ListAppointmentController"})
public class ListAppointmentController extends HttpServlet {

    private PagePathHelper pagePath = null;
    private AppointmentHelper appointmentHelper = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        pagePath = new PagePathHelper();
        appointmentHelper = new AppointmentHelper();
        String page;
        String status = request.getParameter("status");
       
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            List <Appointment> appointments = appointmentHelper.getAppointments(session, status);
            if (appointments.isEmpty()) {
                request.setAttribute("message", "notAppointments");
            } else {
                request.setAttribute("appointments", appointments);
            }
            page = pagePath.getPagePath("listAppointment");
            session.getTransaction().commit();
            session.close();  
        }       
        RequestDispatcher rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }
}
