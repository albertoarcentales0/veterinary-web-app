package controllers;

import helpers.AuthHelper;
import helpers.PagePathHelper;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.HibernateUtil;
import models.Product;
import org.hibernate.Session;

@WebServlet(name = "InsertProductController", urlPatterns = {"/InsertProductController"})
public class InsertProductController extends HttpServlet {
    
    private Product product = null;
    private AuthHelper authHelper = null;
    private PagePathHelper pagePath = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        product = new Product();
        authHelper = new AuthHelper();
        pagePath = new PagePathHelper();
        RequestDispatcher rd;
        String page;
        String description = request.getParameter("description");
        int price = Integer.parseInt(request.getParameter("price"));
        String type = request.getParameter("type");
        String status = request.getParameter("status");

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            boolean isRegisteredProduct = authHelper.isThereAProduct(session, description, type, price, status);
            if (isRegisteredProduct) {
                page = pagePath.getPagePath("insertProduct");
                request.setAttribute("message", "El producto ya existe");
            } else {
                product.setDescription(description);
                product.setType(type);
                product.setPrice(price);
                product.setStatus(status);
                session.save(product); 
                page = pagePath.getPagePath("insertProduct");
                request.setAttribute("message", "El producto fue ingresado");
            }
            session.getTransaction().commit();
            session.close();  
        }        
        rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }
}
