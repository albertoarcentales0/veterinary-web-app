package test;

import helpers.AppointmentHelper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Appointment;
import models.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AppointmentHelperTest {
    
    static Session session = null;
    static Appointment appointment = null;
    static String appointmentID = null;
    static String appointmentStatus = null;
    static String appointmentTime = null;
    static Integer totalAppointments = null;
    static String hour = null;
    
    public AppointmentHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
            Date formattedTime = timeFormatter.parse("11:30:00");
            appointment = new Appointment();
            AppointmentHelperTest.session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            appointment.setOwnerName("owner name test ");
            appointment.setPhoneNumber(123456789);
            appointment.setPetName("pet name test");
            appointment.setType("Tortuga test");
            appointment.setTime(formattedTime);
            appointment.setStatus("pendiente");
            session.save(appointment);
            String hql = "FROM Appointment ORDER BY appointmentID DESC";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            List<Appointment> appointments = query.getResultList();
            appointmentID = Integer.toString(appointments.get(0).getAppointmentID());
            appointmentTime = String.valueOf(appointments.get(0).getTime());
            String[] parts = appointmentTime.split(" ");
            hour = parts[3];  
            appointmentStatus = appointments.get(0).getStatus();
            totalAppointments = appointments.size();
            String hqlTwo = "FROM Appointment a WHERE a.status =:st";
            Query queryTwo = session.createQuery(hqlTwo);
            queryTwo.setParameter("st", "pendiente");
            List<Appointment> appointmentsTwo = queryTwo.getResultList();
            totalAppointments = appointmentsTwo.size();
            session.getTransaction().commit();
            
            
//            String hqlTwo = "FROM Appointment a WHERE a.status =:st";
//            Query queryTwo = session.createQuery(hqlTwo);
//            queryTwo.setParameter("st", "pendiente");
//            List<Appointment> appointmentsTwo = queryTwo.getResultList();
//            totalAppointments = appointmentsTwo.size();
//            appointmentStatus = appointmentsTwo.get(0).getStatus();
//            appointmentTime = String.valueOf(appointmentsTwo.get(0).getTime());
//            session.getTransaction().commit();
        } catch (ParseException ex) {
            Logger.getLogger(AppointmentHelperTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Test of getAppointments method, of class AppointmentHelper.
    @Test
    public void testGetAppointments() {
        System.out.println("getAppointments");
        session.beginTransaction();
        String status = appointmentStatus;
        AppointmentHelper instance = new AppointmentHelper();
        //Quantity of appointments.
        int expResult = totalAppointments;
        List<Appointment> result = instance.getAppointments(session, status);
        assertEquals(expResult, result.size());
    }

    //Test of getTimeValidationMessage method, of class AppointmentHelper.
    @Test
    public void testGetTimeValidationMessage() {
        System.out.println("getTimeValidationMessage");
        String time = hour;
        AppointmentHelper instance = new AppointmentHelper();
        String expResult = "OK";
        String result = instance.getTimeValidationMessage(time);
        assertEquals(expResult, result);
    }

    //Test of isTimeAvailable method, of class AppointmentHelper.
    @Test
    public void testIsTimeAvailable() {
        System.out.println("isTimeAvailable");
        String time = appointmentTime;
        AppointmentHelper instance = new AppointmentHelper();
        boolean expResult = false;
        boolean result = instance.isTimeAvailable(session, time);
        assertEquals(expResult, result);
    }
    
    //Test of deleteAppointment method, of class AppointmentHelper.
    @Test
    public void testDeleteAppointment() {
        System.out.println("deleteAppointment");
        String[] appointmentIDs = { appointmentID };
        String status = "atentido";
        AppointmentHelper instance = new AppointmentHelper();
        int expResult = 1;
        int result = instance.deleteAppointment(session, appointmentIDs, status);
        assertEquals(expResult, result);
        session.getTransaction().commit();
    }
    
    @AfterClass
    public static void afterClass() {
        Integer id = Integer.valueOf(appointmentID);
        session.beginTransaction();
        String hql = "DELETE FROM Appointment WHERE appointmentID=:appointmentID";
        Query query = session.createQuery(hql);
        query.setParameter("appointmentID", id);
        int rowCount = query.executeUpdate();
        if(rowCount == 0) {
            fail("The failure test");
        }
        session.getTransaction().commit();
        session.close();
    }
    
}
