package test;

import helpers.AuthHelper;
import java.util.List;
import models.Employee;
import models.HibernateUtil;
import models.Product;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class AuthHelperTest {
    
    static Session session = null;
    static Product product = null;
    static Integer productID = null;
    static String productDescription = null;
    static String productType = null;
    static String productStatus = null;
    static Integer priceProduct = null;
    static Employee employee = null;
    static Integer employeeID = null;
    static String employeeName = null;
    static String employeePassword = null;
    static Integer totalEmployees = null;
            
    public AuthHelperTest() {
         this.session = HibernateUtil.getSessionFactory().openSession();    
    }
    
    @BeforeClass
    public static void setUpClass() {
        session = HibernateUtil.getSessionFactory().openSession();    
    //Product    
        product = new Product();
        session.beginTransaction();
        product.setDescription("test product");
        product.setType("regular");
        product.setPrice(20);
        product.setStatus("disponible");
        session.save(product);
        
        String hql = "FROM Product ORDER BY productID DESC";
        Query query = session.createQuery(hql);
        query.setMaxResults(1);
        List<Product> products = query.getResultList();
        session.getTransaction().commit();
        productID = products.get(0).getProductID();
        productDescription = products.get(0).getDescription();
        productType = products.get(0).getType();
        productStatus = products.get(0).getStatus();
        priceProduct = products.get(0).getPrice();
    
    //Employee
        employee = new Employee();
        employee.setEmployeeName("employee test");
        employee.setEmployeePassword("employee test");
        employee.setEmployeeRole("role test");
        session.save(employee);
        
        String hqlTwo = "FROM Employee ORDER BY employeeID DESC";
        Query queryTwo = session.createQuery(hqlTwo);
        queryTwo.setMaxResults(1);
        List<Employee> employees = queryTwo.getResultList();
        totalEmployees = employees.size();
        employeeID = employees.get(0).getEmployeeID();
        employeeName = employees.get(0).getEmployeeName();
        employeePassword = employees.get(0).getEmployeePassword();
        
    }
    
    //Test of getEmployees method, of class AuthHelper.
    @Test
    public void testGetEmployees() {
        System.out.println("getEmployees");
        String name = employeeName;
        String password = employeePassword;
        AuthHelper instance = new AuthHelper();
        //Quantity of employees.
        int expResult = totalEmployees;
        List<Employee> result = instance.getEmployees(session, name, password);
        assertEquals(expResult, result.size());
    }

    // Test of isThereAProduct method, of class AuthHelper.
    @Test
    public void testIsThereAProduct() {
        System.out.println("isThereAProduct");
        session.beginTransaction();
        String description = productDescription;
        String type = productType;
        int price = priceProduct;
        String status = productStatus;
        AuthHelper instance = new AuthHelper();
        boolean expResult = true;
        boolean result = instance.isThereAProduct(session, description, type, price, status);
        assertEquals(expResult, result);
        if(!result) {
            fail("The failure test.");
        }
        session.getTransaction().commit();
    }    
    
    @AfterClass
    public static void afterClass() {
        session.beginTransaction();
        //Product
        String hql = "DELETE FROM Product WHERE productID=:productID";
        Query query = session.createQuery(hql);
        query.setParameter("productID", productID);
        int rowCount = query.executeUpdate();
        if(rowCount == 0) {
            fail("The failure test");
        }
        
        //Employee
        String hqlTwo = "DELETE FROM Employee WHERE employeeID=:employeeID";
        Query queryTwo = session.createQuery(hqlTwo);
        queryTwo.setParameter("employeeID", employeeID);
        int rowCountTwo = queryTwo.executeUpdate();
        if(rowCountTwo == 0) {
            fail("The failure test");
        }
        session.getTransaction().commit();
        session.close();  
    }
}
