package test;

import helpers.ProductHelper;
import java.util.List;
import models.HibernateUtil;
import models.Product;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProductHelperTest {
    
    static Session  session = null;
    static Product product = null;
    static String productID = null;
    static String productType = null;
    static String productStatus = null;
    static Integer totalProducts = null;
    
    public ProductHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        ProductHelperTest.session = HibernateUtil.getSessionFactory().openSession();    
        product = new Product();
        session.beginTransaction();
        product.setDescription("test product");
        product.setType("regular");
        product.setPrice(20);
        product.setStatus("disponible");
        session.save(product);
        String hql = "FROM Product ORDER BY productID DESC";
        Query query = session.createQuery(hql);
        query.setMaxResults(1);
        List<Product> products = query.getResultList();
        productID = Integer.toString(products.get(0).getProductID());
        productType = products.get(0).getType();
        productStatus = products.get(0).getStatus();
        totalProducts = products.size();
        session.getTransaction().commit();
    }
    
    //Test of getProducts method, of class ProductHelper.
    @Test
    public void testGetProducts() {
        System.out.println("getProducts");
        String typeOfProduct = productType;
        String status = productStatus;
        ProductHelper instance = new ProductHelper();
        //Quantity of products.
        int expResult = totalProducts;
        List<Product> result = instance.getProducts(session, typeOfProduct, status);
        assertEquals(expResult, result.size());
    }

    //Test of deleteProduct method, of class ProductHelper.
    @Test
    public void testDeleteProduct() {
        System.out.println("deleteProduct");
        session.beginTransaction();
        String[] productIDs = { productID };
        String status = "vendido";
        ProductHelper instance = new ProductHelper();
        int expResult = 1;
        int result = instance.deleteProduct(session, productIDs, status);
        assertEquals(expResult, result);
        session.getTransaction().commit();
    }
    
    @AfterClass
    public static void afterClass() {
        Integer id = Integer.valueOf(productID);
        session.beginTransaction();
        String hql = "DELETE FROM Product WHERE productID=:productID";
        Query query = session.createQuery(hql);
        query.setParameter("productID", id);
        int rowCount = query.executeUpdate();
        if(rowCount == 0) {
            fail("The failure test");
        }
        session.getTransaction().commit();
        session.close();  
    }
    
}
