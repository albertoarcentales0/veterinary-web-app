package test;

import helpers.PagePathHelper;
import static org.junit.Assert.*;

public class PagePathHelperTest {
    
    //Test of getPagePath method, of class PagePathHelper.
    @org.junit.Test
    public void testGetPagePath() {
        System.out.println("getPagePath");
        String key = "sellProduct";
        PagePathHelper instance = new PagePathHelper();
        String expResult = "sellProduct.jsp";
        String result = instance.getPagePath(key);
        assertEquals(expResult, result);
        if(!result.equals(expResult)) {
            fail("The failure test.");
        }
    }
    
}
